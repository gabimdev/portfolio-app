import './About.scss';

const About = () => {
    return (
        <section className="about" id="about">
            <h1 className="about__title">ABOUT ME</h1>
            <p className="about__text">Hi There!</p>
            <p className="about__text">
                I'm Gabriel Miranda, <br />a Full Stack Web Developer based in
                Barcelona.
            </p>
            <br />
            <p className="about__text">
                I am passionate about music, coding and solving problems through
                code, and I am excited to work alongside other amazing
                developers and learn so much more!
            </p>
            <br />
            {/*    <p className="about__text">I'm also a music producer and a dj.</p> */}
        </section>
    );
};

export default About;
