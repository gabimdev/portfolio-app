import { PortfolioCard } from '../../components';
import streamly from '../../assets/streamly.png';
import jobOffer from '../../assets/jobOffer.png';
import portfolio from '../../assets/portfolio.png';
import pluginFx from '../../assets/pluginFx.png';
import checkout from '../../assets/checkout.png';
import './Portfolio.scss';

const Portfolio = () => {
    const projects = [
        {
            name: 'Streamly',
            image: streamly,
            stack: [
                'React',
                'Redux',
                'NodeJs',
                'MongoDB',
                'Axios',
                'Nodemailer',
                'Passport',
            ],
            info: 'Is a service that allows users to track their favourite OTTs and can generate random recommendations based on different criteria.',
            linksLive: 'https://streamly.netlify.app/',
            linksRepo: 'https://gitlab.com/streamly-app',
        },
        {
            name: 'Job Offer',
            image: jobOffer,
            stack: [
                'NodeJs',
                'MongoDB',
                'HBS',
                'Express',
                'Passport',
                'Bootstrap',
            ],
            info: 'Is a service that allows users to create job offers.',
            linksLive: 'http://joboffer-proj.herokuapp.com/',
            linksRepo: 'https://gitlab.com/gabimdev/job-offer-project',
        },
        {
            name: 'Portfolio',
            image: portfolio,
            stack: ['React', 'EmailJs'],
            info: 'My own portfolio which you are looking at right now.',
            linksLive: 'https://gabriel-miranda.netlify.app/',
            linksRepo: 'https://gitlab.com/gabimdev/portfolio-app',
        },
        {
            name: 'Checkout',
            image: checkout,
            stack: ['React', 'validator', 'Sass'],
            info: 'A component to pay with creditcard.',
            linksLive: 'https://gabriel-miranda-checkout-app.netlify.app/',
            linksRepo: 'https://github.com/gabimdev/checkout-app',
        },
        {
            name: 'Plugin Fx',
            image: pluginFx,
            stack: [
                'React',
                'Redux',
                'NodeJs',
                'MongoDB',
                'Express',
                'Passport',
                'Bootstrap',
            ],
            info: 'Is a service to share information about audio plugins processors and effects.',
            linksLive: 'https://plugin-app.netlify.app/',
            linksRepo: 'https://gitlab.com/plugin-app',
        },
    ];
    return (
        <div id="portfolio" className="portfolio">
            <h1 className="portfolio__title">PROJECTS</h1>
            <PortfolioCard projects={projects} />
        </div>
    );
};

export default Portfolio;
