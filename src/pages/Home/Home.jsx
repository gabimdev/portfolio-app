import { BrowserRouter } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import { IoIosArrowDown } from 'react-icons/io';
import './Home.scss';
const Home = () => {
    const scrollWithOffset = (el) => {
        const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
        const yOffset = -120;
        window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' });
    };
    return (
        <section className="home" id="home">
            <div className="home__container">
                <h1 className="home__title">HI, MY NAME IS</h1>
                <h1 className="home__title"> GABRIEL MIRANDA.</h1>
            </div>
            <div className="home__container">
                <h1 className="home__title">I'M A</h1>
                <span className="home__title animate typing "></span>
                <h1 className="home__title">WEB DEVELOPER.</h1>
            </div>
            <div className="home__container">
                <h3 className="home__subtitle">Want to know more?</h3>
            </div>
            <div className="home__arrow">
                <BrowserRouter>
                    <HashLink
                        to="/#about"
                        scroll={(el) => scrollWithOffset(el)}
                    >
                        <IoIosArrowDown className="bounce" />
                    </HashLink>
                </BrowserRouter>
            </div>
        </section>
    );
};

export default Home;
