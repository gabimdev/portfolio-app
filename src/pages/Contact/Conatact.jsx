import { IoMailOutline } from 'react-icons/io5';
import { IoLogoLinkedin } from 'react-icons/io5';
import { IoLogoGitlab } from 'react-icons/io5';
import './Contact.scss';

const Contact = (props) => {
    return (
        <div id="contact" className="contact">
            <div className="contact__container-title">
                <h1 className="contact__title">CONTACT</h1>
            </div>
            <div className="contact__container-main">
                <div className="contact__container" onClick={props.funcView}>
                    <IoMailOutline />
                </div>
                <div className="contact__container">
                    <a
                        href="https://www.linkedin.com/in/gabriel-miranda-fs/"
                        target="_blank"
                        rel="noopener noreferrer"
                        className="contact__link"
                    >
                        <IoLogoLinkedin />
                    </a>
                </div>
                <div className="contact__container">
                    <a
                        href="https://gitlab.com/gabimdev"
                        target="_blank"
                        rel="noopener noreferrer"
                        className="contact__link"
                    >
                        <IoLogoGitlab />
                    </a>
                </div>
            </div>
        </div>
    );
};

export default Contact;
