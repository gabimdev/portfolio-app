import './PortfolioCard.scss';

const PortfolioCard = (props) => {
    return (
        <div className="card">
            {props.projects.map((project) => (
                <div className="card__container-main">
                    <div className="card__container-desktop">
                        <div className="card__container container-margin">
                            <a
                                href={project.linksLive}
                                target="_blank"
                                rel="noopener noreferrer"
                                key={project.linksLive}
                            >
                                <img
                                    src={project.image}
                                    className="card__img"
                                    alt="streamly"
                                />
                            </a>
                        </div>
                    </div>
                    <div className="card__container-desktop">
                        <div className="card__container">
                            <span className="card__title">{project.name}</span>
                        </div>
                        <div className="card__container">
                            <span className="card__stack-list">
                                <ul className="card__list">
                                    {project.stack.map((technology, index) => (
                                        <li
                                            className="card__item"
                                            key={technology}
                                        >
                                            {technology}
                                        </li>
                                    ))}
                                </ul>
                            </span>
                        </div>
                        <div className="card__container">
                            <span className="card__info">{project.info}</span>
                        </div>
                        <div className="card__container">
                            <div className="card__link">
                                <a
                                    href={project.linksLive}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    key={project.linksLive}
                                >
                                    Live
                                </a>
                            </div>
                            <div>
                                <a
                                    href={project.linksRepo}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    key={project.linksLive}
                                >
                                    Repo
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default PortfolioCard;
