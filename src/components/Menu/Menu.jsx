import { HashLink } from 'react-router-hash-link';
import { BrowserRouter } from 'react-router-dom';

import './Menu.scss';

const Menu = (props) => {
    const scrollWithOffset = (el) => {
        const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
        const yOffset = -120;
        window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' });
    };
    return (
        <BrowserRouter>
            <div className={props.open ? 'offcanvas offcanvasOn' : 'offcanvas'}>
                <ul className="offcanvas__list">
                    {props.content.map((link, index) => (
                        <li
                            className="offcanvas__item"
                            key={index + link.title}
                        >
                            {' '}
                            <HashLink
                                className="offcanvas__link-mobile"
                                scroll={(el) => scrollWithOffset(el)}
                                to={link.link}
                                onClick={props.func}
                            >
                                {' '}
                                {link.title}
                            </HashLink>
                            <HashLink
                                className="offcanvas__link-desktop"
                                scroll={(el) => scrollWithOffset(el)}
                                to={link.link}
                            >
                                {' '}
                                <div className="offcanvas__icon">
                                    <spa>{link.icon}</spa>
                                </div>
                                <div className="offcanvas__text">
                                    <span> {link.title}</span>
                                </div>
                            </HashLink>
                        </li>
                    ))}
                </ul>
            </div>
        </BrowserRouter>
    );
};

export default Menu;
