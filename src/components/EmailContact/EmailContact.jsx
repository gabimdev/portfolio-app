import emailjs from 'emailjs-com';
import './EmailContact.scss';

const EmailContact = (props) => {
    function sendEmail(e) {
        e.preventDefault();

        emailjs
            .sendForm(
                process.env.REACT_APP_SERVICE_ID,
                process.env.REACT_APP_TEMPLATE,
                e.target,
                process.env.REACT_APP_USER_ID
            )
            .then(
                (result) => {
                    console.log(result.text);
                    props.funcView();
                },
                (error) => {
                    console.log(error.text);
                }
            );
    }
    return (
        <div className={props.view ? 'outline' : 'outline  hidden'}>
            <div className="emailContact">
                <div className="emailContact__close">
                    <span onClick={props.funcView} className="emailContact__x">
                        X
                    </span>
                </div>
                <h5 className="emailContact__title">CONTACT FORM</h5>
                <form className="form" onSubmit={sendEmail}>
                    <input type="hidden" name="contact_number" />
                    <div className="form__fieldset">
                        <input
                            type="email"
                            name="user_email"
                            className="form__control"
                            id="email"
                            placeholder="Your email"
                        />
                    </div>
                    <div className="form__fieldset">
                        <input
                            type="text"
                            name="user_name"
                            className="form__control"
                            id="name"
                            placeholder="Your name"
                        />
                    </div>
                    <div className="form__fieldset">
                        <textarea
                            className="form__control-textarea"
                            id="message"
                            name="message"
                            placeholder="Your message"
                        ></textarea>
                    </div>
                    <button type="submit" className="btn">
                        SEND
                    </button>
                </form>
            </div>
        </div>
    );
};

export default EmailContact;
