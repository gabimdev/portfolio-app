import './Header.scss';

const Header = (props) => {
    return (
        <nav className="navbar">
            <div className="navbar__menu">
                <div
                    className="menu-icon"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasScrolling"
                    aria-controls="offcanvasScrolling"
                >
                    {
                        <input
                            className={'menu-icon__cheeckbox'}
                            checked={props.open}
                            defaultChecked
                            type="checkbox"
                            onClick={props.func}
                        />
                    }
                    <div>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </nav>
    );
};

export default Header;
