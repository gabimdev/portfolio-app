import { useState, useEffect } from 'react';
import ReactGA from 'react-ga';
import { EmailContact, Header, Menu } from './components';
import { Home, About, Portfolio, Contact } from './pages';
import { ImHome3 } from 'react-icons/im';
import { IoPerson } from 'react-icons/io5';
import { IoCodeWorkingSharp } from 'react-icons/io5';
import { IoShareSocialSharp } from 'react-icons/io5';

import './App.scss';

function App() {
    useEffect(() => {
        ReactGA.initialize('UA-213962581-1');
        ReactGA.send({ hitType: 'pageview', page: '/' });
    }, []);

    const [isOpen, setIsOpen] = useState(false);
    const [viewForm, setViewForm] = useState(false);
    const menuOpen = () => {
        setIsOpen(!isOpen);
    };

    const handleView = () => {
        setViewForm(!viewForm);
    };

    const pageContent = [
        {
            title: 'Home',
            link: '/#home',
            icon: <ImHome3 />,
        },
        {
            title: 'About',
            link: '/#about',
            icon: <IoPerson />,
        },
        {
            title: 'Projects',
            link: '/#portfolio',
            icon: <IoCodeWorkingSharp />,
        },
        {
            title: 'Contact',
            link: '/#contact',
            icon: <IoShareSocialSharp />,
        },
    ];
    return (
        <div className="app">
            <EmailContact funcView={handleView} view={viewForm} />
            <Header className="app__header" func={menuOpen} open={isOpen} />
            <Menu open={isOpen} content={pageContent} func={menuOpen} />
            <div className="app__container">
                <Home />
                <About />
                <Portfolio />
                <Contact funcView={handleView} />
            </div>
        </div>
    );
}

export default App;
